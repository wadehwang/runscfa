#!/pkg/biology/Python/Python2_default/bin/python
def merge_results(filepath_pattern,outputfile):
        import glob as glob
        files=glob.glob(filepath_pattern)
        output=pd.DataFrame()
        for file in files:
                record=pd.read_table(file)
                output=pd.concat([output,record],ignore_index=True)
        output.to_csv(outputfile,sep="\t",index=False)

def downloadrun(run,outdir):
    import getpass
    import os
    outrundir=outdir+'/runs/'+run['Run']
    Filename=run['download_path'].split('/')[-1]
    username = getpass.getuser()
    cwd=os.getcwd()
    CMD=''
    mkdirCMD='mkdir -p '+outrundir
    downloadCMD=''
    softlinkCMD=''
    
    if run['Run'][0:3]=='ERR':
            print "Using ascp to download data"
            #downloadCMD=ascpdownloadERRrun(run['Run'],outrundir) #for ascp donwload
            downloadCMD='wget --retry-connrefused --waitretry=1 -t 10 -c -P %s %s' % (outrundir,run['download_path']) #for http download
            if Filename !=run['Run']:
                softlinkCMD='ln -fs ./%s %s/%s' % (Filename,outrundir,run['Run']) #for http download
    else:
            downloadCMD='wget --retry-connrefused --waitretry=1 -t 10 -c -P %s %s' % (outrundir,run['download_path'])
            if Filename !=run['Run']:
                    softlinkCMD='ln -fs ./%s %s/%s' % (Filename,outrundir,run['Run'])

    sshdownloadCMD='ssh -tt  %s@clogin4 \'cd %s; %s\'' % (username,cwd,downloadCMD)
    CMD='\n'.join((mkdirCMD,sshdownloadCMD,softlinkCMD))
    return CMD

def ascpdownloadERRrun(ERRid,outrundir):
    #Need to open port 33001 to work
    folder='/vol1/err/'+ERRid[0:6]+'/00'+ERRid[-1]+'/'+ERRid
    downloadCMD='/pkg/biology/Aspera/Aspera_v3.7.7/cli/bin/ascp -P33001 -O33001 -k1 -QT -l 100m -i /pkg/biology/Aspera/Aspera_v3.7.7/cli/etc/asperaweb_id_dsa.openssh era-fasp@fasp.sra.ebi.ac.uk:%s %s' % (folder,outrundir)
    return downloadCMD
     
def ascpdonwloadSRRrun(SRRid,outdir):
    pass

def fastqdump(fastqdump_path,run,outdir,thread):
    outrundir=outdir+'/runs/'+run['Run']
    fastqdir=outdir+'/fastqs'
    Filename=run['download_path'].split('/')[-1]
    CMD='export PATH=/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin:$PATH ; mkdir -p %s \n' % (fastqdir)
    CMD=CMD+'cd %s; %s --gzip --split-e %s ;cd - \n' % (outrundir,fastqdump_path,run['Run'])  
    CMD=CMD+'cd %s ;ln -fs ../runs/%s/*fastq.gz ./ ; cd - \n' % (fastqdir,run['Run'])
    return CMD 
    #return ''

def pfastqdump(pfastqdump_path,run,outdir,thread):
    outrundir=outdir+'/runs/'+run['Run']
    fastqdir=outdir+'/fastqs'
    Filename=run['download_path'].split('/')[-1]
    CMD='export PATH=/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin:$PATH ; mkdir -p %s \n' % (fastqdir)
    if run['LibraryLayout']=='PAIRED':
        CMD=CMD+'cd %s; %s --gzip --split-e -s %s -t %s ;cd - \n' % (outrundir,pfastqdump_path,run['Run'],thread)

    if run['LibraryLayout']!='PAIRED':
            CMD=CMD+'cd %s; %s --gzip -s %s ;cd - \n' % (outrundir,pfastqdump_path,run['Run'])

    CMD=CMD+'cd %s ;ln -fs ../runs/%s/*fastq.gz ./ ; cd - \n' % (fastqdir,run['Run'])
    return CMD

def SCFA(SCFA_path,inputdir,outdir,thread,diamondDB):
    SCFA_CMD='export PATH=/pkg/biology/Python/Python3_default/bin:/pkg/biology/Run_SCFA/runscfa_20200115/bin:$PATH \n'
    SCFA_CMD=SCFA_CMD+'mkdir -p %s \n cd %s \n' % (outdir,outdir)
    SCFA_CMD=SCFA_CMD+'%s -i %s -p %s -d %s -o %s\n' % (SCFA_path,inputdir,thread,diamondDB,outdir)
    return SCFA_CMD
    #return ''



def Propionate(Propionate_path,inputdir,outdir,thread,diamondDB):
    Propionate_CMD='export PATH=/pkg/biology/Python/Python3_default/bin:/pkg/biology/Run_SCFA/runscfa_20200115/bin_Propionate:$PATH \n'
    Propionate_CMD=Propionate_CMD+'mkdir -p %s \n cd %s \n' % (outdir,outdir)
    Propionate_CMD=Propionate_CMD+'%s -i %s -p %s -d %s -o %s\n' % (Propionate_path,inputdir,thread,diamondDB,outdir)
    return Propionate_CMD
    #return ''

def SOAP(SOAP_path,inputdir,outdir,thread,diamondDB):
    SOAP_CMD='export PATH=/pkg/biology/Python/Python3_default/bin:/pkg/biology/Run_SCFA/runscfa_20200115/bin_SOAP:/pkg/biology/SOAP2/SOAP2_v2.21:$PATH \n'
    SOAP_CMD=SOAP_CMD+'mkdir -p %s \n cd %s \n' % (outdir,outdir)
    SOAP_CMD=SOAP_CMD+'%s -i %s -d %s -o %s\n' % (SOAP_path,inputdir,diamondDB,outdir)
    return SOAP_CMD
    #return ''
