#!/pkg/biology/Python/Python2_default/bin/python
class Job:
    def __init__(self, queue,projectID,thisjobname,logdir,command,dependency,verbose,MaxWaitingJobNumber,prevjob,resume):
        self.queue = queue
        self.projectID = projectID
        self.jobname = thisjobname
        self.logdir = logdir
        self.command = command
        self.dependency = dependency
        self.verbose = verbose
        self.MaxWaitingJobNumber = MaxWaitingJobNumber
        self.resume = resume
        self.prevjob = prevjob
        self.torun = True
        self.nextdependency=''
        import getpass
        self.username = getpass.getuser()
        if self.resume:
            (self.torun,self.thisdependency,self.nextdependency)=self.resumecheck()
            if type(self.dependency)=='str':
                self.dependency=self.thisdependency
            if type(self.dependency)=='list':
                self.dependency["jobs"]=self.thisdependency

    def submit(self):
        from popen2 import popen2
        import os
        import time        
        queue_corenumber_dict={
            'NCHC_16G':'4',
            'NCHC_180G':'40',
            'NCHC_4G':'1',
            'ngs4G':'1',
            'ngs16G':'4',
            'ngs48G':'10',
            'ngs192G':'40'
        } 
        
        if not self.torun:
            self.info()
            return self.nextdependency
          
        self.dependency_setting=''
        if self.dependency:
            if type(self.dependency["jobs"])==list:
                print self.dependency
                self.dependency_setting="#PBS -W depend="+self.dependency["type"]+":"+':'.join(self.dependency["jobs"])
            else:
                self.dependency_setting="#PBS -W depend="+self.dependency["type"]+":"+self.dependency["jobs"]
        
        if self.MaxWaitingJobNumber :
            while self.getjobnumber('') > self.MaxWaitingJobNumber:
                time.sleep(10)
    
        
        output, input = popen2('qsub')
        # Customize your options here
        self.processors = 'nodes=1:ppn='+queue_corenumber_dict[self.queue]
        self.job_string = """#!/bin/bash
#PBS -q %s
#PBS -P %s
#PBS -W group_list=%s
#PBS -N %s
#PBS -l %s
#PBS -o %s/%s.out
#PBS -e %s/%s.err
%s    
    
cd $PBS_O_WORKDIR

mkdir -p %s
%s""" % (self.queue, self.projectID , self.projectID , self.jobname,  self.processors, self.logdir,self.jobname, self.logdir,self.jobname,self.dependency_setting,self.logdir,self.command)
     
        # Send job_string to qsub
        
        input.write(self.job_string)
        input.close()
        print self.job_string
    
        # Print your job and the system response to the screen as it's submitted
        os.system('mkdir -p ./logs')
        with open('%s/%s.pbs' % (self.logdir,self.jobname), 'w') as script:
            script.write(self.job_string)
        self.nextdependency=output.read().strip()
        self.thisdependency=self.dependency
        if self.verbose :
                     print "#####Job script#####\n"+self.job_string+"\n#####Jobscript End##############\n"
                     self.info()
        time.sleep(0.1)

        return self.nextdependency
    def info(self):
        previous_jobname=''
        previous_jobstatus=''
        if self.prevjob:
            previous_jobname=self.prevjob.jobname
            previous_jobstatus=self.prevjob.getjobstatus('')
        else:
            previous_jobname=self.prevjob
        print '''
Checking resume information For
        --Jobname: %s
        --Status: %s
        --ToRun: %s
        --Previous Job: %s Status: %s
        --Dependency of this Job: %s
        --Next Jobdepedency: %s
            ''' % ( self.jobname,self.getjobstatus(''),str(self.torun),previous_jobname,previous_jobstatus,str(self.thisdependency),self.nextdependency)

    def getjobnumber(self,jobnamepattern):
        import subprocess
        if jobnamepattern:
            pass
        else:
            jobnamepattern=self.username
        self.getnumcommand= 'qstat -w -u %s|grep %s|wc -l' %  (self.username,jobnamepattern)
        
        try:
            self.getnumoutput = subprocess.check_output(self.getnumcommand,shell=True).strip()
        
        except subprocess.CalledProcessError as e:
            self.getnumoutput = e.output       # Output generated before error
            self.getnumcode   = e.returncode   # Return code
            #    sys.exit(output)
            
        return int(self.getnumoutput)



    def getjobstatus(self,errorpattern):
        #This will return "ERR" if Error message in the log file. If the log file not exists , "NOTINIT" will be returned. If job is running, "RUN" will be returned
        import subprocess
        getjobexe = "/pkg/biology/Run_SCFA/workflow_on_pbs/get_job_status.pl"
        command = '%s %s | grep %s | wc -l' % ( getjobexe,self.username,self.jobname )    
        output = int(subprocess.check_output(command,shell=True).strip())
        
        if output > 0:
            return "RUN"
        if not errorpattern:
            errorpattern='error|ERROR|Error|err'
            command='grep -E \'%s\' %s/%s.err|wc -l' %(errorpattern,self.logdir,self.jobname)  
        
        try:
            
            errfile=open('%s/%s.err' %(self.logdir,self.jobname),'r')
            
            output = subprocess.check_output(command,shell=True).strip()
        
            if int(output) > 0:
                return "ERR"
            else:
                return "NOERR"

        except:
            return "NOTINIT"

    def getjobid(self):   
        import subprocess
        self.getjobexe = "/pkg/biology/Run_SCFA/workflow_on_pbs/get_job_status.pl"
        self.getidcommand = '%s %s | grep %s | head -1 |cut -f 2' % ( self.getjobexe , self.username , self.jobname )
        self.getidoutput = subprocess.check_output(self.getidcommand,shell=True).strip()
        return self.getidoutput

    def resumecheck(self):
        self.previous_jobstatus=''
        if self.prevjob:
            self.previous_jobstatus=self.prevjob.getjobstatus('')
            
        self.thisjobstatus=self.getjobstatus('')
        self.torun=False
        #####If job is RUNNING, do not run again. Next job will depend on running job.
        
        if self.thisjobstatus == "RUN":    
            self.torun=False; 
            self.thisdependency='';
            self.nextdependency=self.getjobid()
        #####If job is NOT INITIATED, run the jobs. Next job will depend on this job.
        if self.thisjobstatus == "NOTINIT": 
            self.torun=True;  
            self.nextdependency=''
            ####if the job is first job,there is no previous job for setting dependency
            if not self.prevjob:
                self.thisdependency=True
            else:
                self.thisdependency=self.prevjob.getjobid()
        #####if job has NO ERROR in previous run, do not run the job. Next job will depend on previous job
        if self.thisjobstatus == "NOERR":  
            self.torun=False; 
            self.thisdependency='';
            ####if the job is first job,there is no previous job for setting dependency
            if not self.prevjob:
                self.nextdependency=''
            else:
                self.nextdependency=self.prevjob.getjobid()
        #####If job has ERROR in previous run, run the jobs. Next job will depend on this job.
        if self.thisjobstatus == "ERR" or self.previous_jobstatus=="RUN":
            self.torun=True;
            self.nextdependency=''
            ####if the job is first job,there is no previous job for setting dependency
            if not self.prevjob:
                self.thisdependency=True
            else:
                self.thisdependency=self.prevjob.getjobid()
        return (self.torun,self.thisdependency,self.nextdependency)






















