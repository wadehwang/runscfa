#!/pkg/biology/Python/Python2_default/bin/python
import sys
import os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../")
import getpass
from NCHC.queue.pbs import Job
username = getpass.getuser()
argv=sys.argv
projectID='MST108173'
outdir=os.getcwd()
verbose=False

if '--outdir' in argv:
    outdir=argv[argv.index('--outdir')+1]
if outdir=='./':
    outdir=os.getcwd()
if '--project' in argv:
    projectID=argv[argv.index('--project')+1]
    #download run lists from projectID
if '-v' in argv:
    verbose=True
if '--help' in argv:
    print """
USAGE:
        pipeline.py module --sraproject [NCBI Project ID]
OPTIONS:

        module  SCFA,Propionate,SOAP

        --sraproject
            Project ID in NCBI format.
        --runids
            Only these run IDs will be run if this option presents.
        --outdir
            Output directory where the results locate. Default is current working directory
        --project
            Project ID for submitting jobs. Default: MST108173
        --test
            Testing mode: Only one run will be run for testing.
        --SCFAonly --Propionateonly --SOAPonly
            Run analysis only,skipping SRA download and fastqdump.
        --inputdir
            The input directory of data that is self-uploaded (not from NCBI or EBI)
        --downloadonly
            Run only dowload and fastqdump
        --resume
            Resume the job.
        -v
            Verbose

EXAMPLES of SCFA:
        If data is from a ncbi project:
            pipeline.py SCFA --sraproject ERP012177
        To run only one run for testing:
            pipeline.py SCFA --sraproject ERP012177 --test
        If only a run in a project to be run:
            pipeline.py SCFA --sraproject ERP012177 --runids ERR1018197
        If two runs in a project to be run:
            pipeline.py SCFA --sraproject ERP012177 --runids ERR1018197,ERR1018196
        If only the SCFA step to be run( skipping download and fastqdump):
            pipeline.py SCFA --sraproject ERP012177 --runids ERR1018197,ERR1018196 --SCFAonly
        If input data is self-uploaded, not from NCBI or EBI. Each run's fastq.gz should be put into a directory named with its run ID.
            pipeline.py SCFA --inputdir [data directory]


        """

    sys.exit(0)
if '--inputdir' not in argv and '--sraproject' not in argv:
    print "Please specify at least --inputdir or --SCFAonly"
    sys.exit(0)


script_path = os.path.dirname(os.path.realpath(__file__))
argv=sys.argv
arguements=" ".join(argv[2:])
module=argv[1]

if module=="SCFA":
    CMD="ssh -tt %s@clogin4 \'cd %s; %s/scripts/SCFA.py %s\'" % (username,outdir,script_path,arguements)
elif module=="Propionate" :
    CMD="ssh -tt %s@clogin4 \'cd %s; %s/scripts/Propionate.py %s\'" % (username,outdir,script_path,arguements)
elif module=="SOAP" :
    CMD="ssh -tt %s@clogin4 \'cd %s; %s/scripts/SOAP.py %s\'" % (username,outdir,script_path,arguements)
else :
    print "please specifiy module name: \"SCFA\",\"Propionate\" or \"SOAP\""
    sys.exit(0)
job=Job('ngs4G',projectID,'Jobmanager.'+module,outdir+'/logs',CMD,'','','','','')
jobid=job.submit()


