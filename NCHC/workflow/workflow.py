#!/pkg/biology/Python/Python2_default/bin/python
def SCFA_only_wf(inputdir,outdir,thread,DiamondDB_path,verbose,MaxWaitingJobNumber):
        filelist=os.listdir(inputdir)
        all_jobids=[]
        count=0
        for run in filelist:
                if runIDs and run not in runIDs:
                        continue
                fullPath = os.path.join(inputdir, run)
                SCFA_jobid=''
                if os.path.isdir(fullPath):                        
                        SCFA_jobid=pbs.jobsubmit('ngs48G',projectID,run+'.3.run_SCFA.SCFA',outdir,SCFA(inputdir+'/'+run,outdir+'/results/'+run,thread,DiamondDB_path),'',verbose,MaxWaitingJobNumber)
                        all_jobids.append(SCFA_jobid)
                        count=count+1
                if count == testtimes:
                        break
                merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/results/*/result.txt\' --outfile %s/result.txt --sortindex %s \n' % (script_path,outdir,outdir,'sample')
                merge_jobid=pbs.jobsubmit('ngs4G',projectID,run+'.4.Merge_results.SCFA',outdir,merge_CMD,{"jobs":SCFA_jobid,"type":"afterany"},verbose,'')

def workflow1(run,outdir,inputdir,thread):
        CMD=fastqdump(run,outdir,thread)
        CMD=CMD+SCFA(inputdir,thread,diamondDB)
        return fastqdump_CMD

def SCFA(argv,MaxWaitingJobNumber):
        import os,subprocess,sys,csv
        from popen2 import popen2
        import time
        import getpass
        import pandas as pd
        import glob
        import NCHC.step.step as step
        import NCHC.workflow.workflow as workflow
        import NCHC.queue.pbs as pbs
        import getpass
        from NCHC.queue.pbs import Job
        username = getpass.getuser()
        
        #argv=sys.argv
        #fastqdumpPath='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        script_path = os.path.dirname(os.path.realpath(__file__))
        pfastqdump_path='/pkg/biology/pfastq-dump/pfastqdump_v0.1.6/bin/pfastq-dump' #path
        fastqdump_path='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        DiamondDB_path='/pkg/biology/Run_SCFA/runscfa_20200115/Diamond_DB/SCFA/All_prot.fasta'
        SCFA_path='/pkg/biology/Run_SCFA/runscfa_20200115/bin/Run_pathwayCount.py'
        verbose=False
        thread=10
        outdir=os.getcwd()
        inputdir=os.getcwd()
        projectID='MST108173'
        sraprojectID='ERP012177'
        runIDs=[]
        testtimes=0
        resume=False
        if '--thread' in argv:
                thread=argv[argv.index('--thread')+1]
        if '--outdir' in argv:
                outdir=argv[argv.index('--outdir')+1]
        if outdir=='./':
                outdir=os.getcwd()
        if '--project' in argv:
                projectID=argv[argv.index('--project')+1]
                #download run lists from projectID
        if '--sraproject' in argv:
                sraprojectID=argv[argv.index('--sraproject')+1]
                outdir=outdir+'/'+sraprojectID.strip()
                os.system('mkdir -p %s' % outdir)
                os.chdir(outdir)
        if '--test' in argv:
                #        testtimes=int(argv[argv.index('--test')+1])
                testtimes=2
                print 'Test mode: Times '+str(testtimes)
        if '--runids' in argv:
                runIDs=argv[argv.index('--runids')+1].split(',')
                print runIDs
        if '--resume' in argv:
                resume=True
        if '-v' in argv:
                verbose=True
        if '--inputdir' in argv:
                inputdir=os.path.abspath(argv[argv.index('--inputdir')+1])

        if '--help' in argv:
                print """
USAGE: 
        pipeline.py --sraproject [NCBI Project ID] 
OPTIONS:        
        --sraproject
            Project ID in NCBI format. 
        --runids
            Only these run IDs will be run if this option presents.
        --outdir
            Output directory where the results locate. Default is current working directory
        --project 
            Project ID for submitting jobs. Default: MST108173
        --test 
            Testing mode: Only one run will be run for testing. 
        --SCFAonly
            Run SCFA only,skipping SRA download and fastqdump.
        --inputdir
            The input directory of data that is self-uploaded (not from NCBI or EBI)
        --resume
            resume the unfinished or redo the error jobs
        -v
            Verbose

EXAMPLES:
        If data is from a ncbi project:
            pipeline.py --sraproject ERP012177
        To run only one run for testing:
            pipeline.py --sraproject ERP012177 --test
        If only a run in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197
        If two runs in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196
        If only the SCFA step to be run( skipping download and fastqdump):
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196 --SCFAonly
        If input data is self-uploaded, not from NCBI or EBI. Each run's fastq.gz should be put into a directory named with its run ID.
            pipeline.py --inputdir [data directory]        

        """

                sys.exit(0)
        if '--inputdir' not in argv and '--sraproject' not in argv:
                print "Please specify at least --inputdir or --SCFAonly"
                sys.exit(0)
       
        if '--inputdir' in argv:
                workflow.SCFA_only_wf(inputdir,outdir,thread,DiamondDB_path,verbose,MaxWaitingJobNumber)
                sys.exit(0)
        
        #getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s[BioProject]\' -O -' % (sraprojectID)
        getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s\' -O -' % (sraprojectID)
        list = subprocess.check_output(getlistcmd, shell=True).splitlines()
        runsReader=csv.DictReader(list)        
        download_jobids=[]
        all_jobids=[]
        count=0
        download_thread=6 #number of download jobs at the same time
        for run in runsReader:
                SCFA_result_folder='%s/results_SCFA' % outdir
                logdir=outdir+'/logs'
                if runIDs and run['Run'] not in runIDs:
                        continue
                if '--SCFAonly' in argv:
                        print 'submitting jobs for %s' % run['Run']
                        SCFA_job=Job('ngs48G',projectID,run['Run']+'.3.run_SCFA.SCFA',logdir,step.SCFA(SCFA_path,outdir+'/runs/'+run['Run'],SCFA_result_folder+'/'+run['Run'],thread,DiamondDB_path),'',verbose,MaxWaitingJobNumber,'',resume)
                        SCFA_jobid=SCFA_job.submit()
                        all_jobids.append(SCFA_jobid)
                        merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/result.txt\' --outfile %s/result.txt --sortindex %s \n' % (script_path,SCFA_result_folder,outdir,'sample')
                        merge_CMD=merge_CMD+'%s/../../scripts/mergeresults.py --filepath \'%s/*/result_gene.txt\' --outfile %s/result_gene.txt --sortindex %s \n' % (script_path,SCFA_result_folder,outdir,'sample')
                        merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.SCFA',logdir,merge_CMD,{"jobs":SCFA_jobid,"type":"afterany"},verbose,'','','')
                        merge_jobid=merge_job.submit()
                        count=count+1
                        if count == testtimes:
                                break
                        continue
                            
###download sample
                dep_index=count-download_thread

                print 'submitting jobs for %s' % run['Run']
                
                #following while loop should be used while dependency issue hasn't been fixed.
             
                if dep_index <0:
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.SCFA',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)
                else:                
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.SCFA',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)

                download_nextdep_jobid=download_job.submit()
                last_download_jobid=download_nextdep_jobid
                download_jobids.append(download_nextdep_jobid)
                
                ###if download finished,then submitjob


                ####fastqdump ID
                
                dependency={"jobs":download_nextdep_jobid,"type":"afterok"}
                fastqdump_cmd=step.fastqdump(fastqdump_path,run,outdir,thread)
                pfastqdump_cmd=step.pfastqdump(pfastqdump_path,run,outdir,thread)
                fastqdump_job=Job('ngs16G',projectID,run['Run']+'.2.fastqdump.SCFA',logdir,fastqdump_cmd,dependency,verbose,'',download_job,resume)
                fastqdump_nextdep_jobid=fastqdump_job.submit()
                if '--downloadonly' in argv:  all_jobids.append(fastqdump_nextdep_jobid); continue
                               
                ####SCFA ID

                dependency={"jobs":fastqdump_nextdep_jobid,"type":"afterok"} 
                SCFA_job=Job('ngs48G',projectID,run['Run']+'.3.run_SCFA.SCFA',logdir,step.SCFA(SCFA_path,outdir+'/runs/'+run['Run'],SCFA_result_folder+'/'+run['Run'],thread,DiamondDB_path),dependency,verbose,'',fastqdump_job,resume)
                SCFA_nextdep_jobid=SCFA_job.submit()
                print "SCFA jobid: "+SCFA_nextdep_jobid
                all_jobids.append(SCFA_nextdep_jobid)
                


                ####merge results
                merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/result.txt\' --outfile %s/result.txt --sortindex %s \n' % (script_path,SCFA_result_folder,outdir,'sample')
                merge_CMD=merge_CMD+'%s/../../scripts/mergeresults.py --filepath \'%s/*/result_gene.txt\' --outfile %s/result_gene.txt --sortindex %s \n' % (script_path,SCFA_result_folder,outdir,'sample')
                all_jobids=filter(None,all_jobids)
#                dependency={"jobs":all_jobids,"type":"afterany"}
                dependency={"jobs":SCFA_nextdep_jobid,"type":"afterany"}
                merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.SCFA',logdir,merge_CMD,dependency,verbose,'','','')
                merge_nextdep_jobid=merge_job.submit()

                count=count+1
                if count == testtimes:
                        break

def Propionate(argv,MaxWaitingJobNumber):
        import os,subprocess,sys,csv
        from popen2 import popen2
        import time
        import getpass
        import pandas as pd
        import glob
        import NCHC.step.step as step
        import NCHC.workflow.workflow as workflow
        import NCHC.queue.pbs as pbs
        import getpass
        from NCHC.queue.pbs import Job
        username = getpass.getuser()
        
        #argv=sys.argv
        #fastqdumpPath='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        script_path = os.path.dirname(os.path.realpath(__file__))
        pfastqdump_path='/pkg/biology/pfastq-dump/pfastqdump_v0.1.6/bin/pfastq-dump' #path
        fastqdump_path='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        DiamondDB_path='/pkg/biology/Run_SCFA/runscfa_20200115/Diamond_DB/Propionate/Propionate.fasta'
        Propionate_path='/pkg/biology/Run_SCFA/runscfa_20200115/bin_Propionate/Run_pathwayCount_Propionate.py'
        verbose=False
        thread=10
        outdir=os.getcwd()
        inputdir=os.getcwd()
        projectID='MST108173'
        sraprojectID='ERP012177'
        runIDs=[]
        testtimes=0
        resume=False
        if '--thread' in argv:
                thread=argv[argv.index('--thread')+1]
        if '--outdir' in argv:
                outdir=argv[argv.index('--outdir')+1]
        if outdir=='./':
                outdir=os.getcwd()
        if '--project' in argv:
                projectID=argv[argv.index('--project')+1]
                #download run lists from projectID
        if '--sraproject' in argv:
                sraprojectID=argv[argv.index('--sraproject')+1]
                outdir=outdir+'/'+sraprojectID.strip()
                os.system('mkdir -p %s' % outdir)
                os.chdir(outdir)
        if '--test' in argv:
                #        testtimes=int(argv[argv.index('--test')+1])
                testtimes=2
                print 'Test mode: Times '+str(testtimes)
        if '--runids' in argv:
                runIDs=argv[argv.index('--runids')+1].split(',')
                print runIDs
        if '--resume' in argv:
                resume=True
        if '-v' in argv:
                verbose=True
        if '--inputdir' in argv:
                inputdir=os.path.abspath(argv[argv.index('--inputdir')+1])

        if '--help' in argv:
                print """
USAGE:
        pipeline.py --sraproject [NCBI Project ID]
OPTIONS:
        --sraproject
            Project ID in NCBI format.
        --runids
            Only these run IDs will be run if this option presents.
        --outdir
            Output directory where the results locate. Default is current working directory
        --project
            Project ID for submitting jobs. Default: MST108173
        --test
            Testing mode: Only one run will be run for testing.
        --Propionateonly
            Run Propionate only,skipping SRA download and fastqdump.
        --inputdir
            The input directory of data that is self-uploaded (not from NCBI or EBI)
        --resume
            resume the unfinished or redo the error jobs
        -v
            Verbose

EXAMPLES:
        If data is from a ncbi project:
            pipeline.py --sraproject ERP012177
        To run only one run for testing:
            pipeline.py --sraproject ERP012177 --test
        If only a run in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197
        If two runs in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196
        If only the Propionate step to be run( skipping download and fastqdump):
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196 --Propionateonly
        If input data is self-uploaded, not from NCBI or EBI. Each run's fastq.gz should be put into a directory named with its run ID.
            pipeline.py --inputdir [data directory]

        """

                sys.exit(0)
        if '--inputdir' not in argv and '--sraproject' not in argv:
                print "Please specify at least --inputdir or --Propionateonly"
                sys.exit(0)
       
        if '--inputdir' in argv:
                workflow.Propionate_only_wf(inputdir,outdir,thread,DiamondDB_path,verbose,MaxWaitingJobNumber)
                sys.exit(0)
        
        #getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s[BioProject]\' -O -' % (sraprojectID)
        getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s\' -O -' % (sraprojectID)
        list = subprocess.check_output(getlistcmd, shell=True).splitlines()
        runsReader=csv.DictReader(list)
        download_jobids=[]
        all_jobids=[]
        count=0
        download_thread=6 #number of download jobs at the same time
        for run in runsReader:
                propionate_result_folder='%s/results_propionate' % outdir
                logdir=outdir+'/logs'
                if runIDs and run['Run'] not in runIDs:
                        continue
                if ('--propionateonly' in argv) or ('--Propionateonly' in argv):
                        print 'submitting jobs for %s' % run['Run']
                        Propionate_job=Job('ngs48G',projectID,run['Run']+'.3.run_Propionate.Propionate',logdir,step.Propionate(Propionate_path,outdir+'/runs/'+run['Run'],propionate_result_folder+'/'+run['Run'],thread,DiamondDB_path),'',verbose,MaxWaitingJobNumber,'',resume)
                        Propionate_jobid=Propionate_job.submit()
                        all_jobids.append(Propionate_jobid)
                        merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/Propionate_result.txt\' --outfile %s/Propionate_result.txt --sortindex %s \n' % (script_path,propionate_result_folder,outdir,'sample')
                        merge_CMD=merge_CMD+'%s/../../scripts/mergeresults.py --filepath \'%s/*/Propionate_result_gene.txt\' --outfile %s/Propionate_result_gene.txt --sortindex %s \n' % (script_path,propionate_result_folder,outdir,'sample')
                        merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.Propionate',logdir,merge_CMD,{"jobs":Propionate_jobid,"type":"afterany"},verbose,'','','')
                        merge_jobid=merge_job.submit()
                        count=count+1
                        if count == testtimes:
                                break
                        continue
                            
###download sample
                dep_index=count-download_thread

                print 'submitting jobs for %s' % run['Run']
                
                #following while loop should be used while dependency issue hasn't been fixed.
             
                if dep_index <0:
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.Propionate',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)
                else:
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.Propionate',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)

                download_nextdep_jobid=download_job.submit()
                last_download_jobid=download_nextdep_jobid
                download_jobids.append(download_nextdep_jobid)
                
                ###if download finished,then submitjob


                ####fastqdump ID
                
                dependency={"jobs":download_nextdep_jobid,"type":"afterok"}
                fastqdump_cmd=step.fastqdump(fastqdump_path,run,outdir,thread)
                pfastqdump_cmd=step.pfastqdump(pfastqdump_path,run,outdir,thread)
                fastqdump_job=Job('ngs16G',projectID,run['Run']+'.2.fastqdump.Propionate',logdir,fastqdump_cmd,dependency,verbose,'',download_job,resume)
                fastqdump_nextdep_jobid=fastqdump_job.submit()
                if '--downloadonly' in argv:  all_jobids.append(fastqdump_nextdep_jobid); continue
                               
                ####Propionate ID

                dependency={"jobs":fastqdump_nextdep_jobid,"type":"afterok"}
                Propionate_job=Job('ngs48G',projectID,run['Run']+'.3.run_Propionate.Propionate',logdir,step.Propionate(Propionate_path,outdir+'/runs/'+run['Run'],propionate_result_folder+'/'+run['Run'],thread,DiamondDB_path),dependency,verbose,'',fastqdump_job,resume)
                Propionate_nextdep_jobid=Propionate_job.submit()
                print "Propionate jobid: "+Propionate_nextdep_jobid
                all_jobids.append(Propionate_nextdep_jobid)
                


                ####merge results
                merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/Propionate_result.txt\' --outfile %s/Propionate_result.txt --sortindex %s \n' % (script_path,propionate_result_folder,outdir,'sample')
                merge_CMD=merge_CMD+'%s/../../scripts/mergeresults.py --filepath \'%s/*/Propionate_result_gene.txt\' --outfile %s/Propionate_result_gene.txt --sortindex %s \n' % (script_path,propionate_result_folder,outdir,'sample')
                all_jobids=filter(None,all_jobids)
#                dependency={"jobs":all_jobids,"type":"afterany"}
                dependency={"jobs":Propionate_nextdep_jobid,"type":"afterany"}
                merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.Propionate',logdir,merge_CMD,dependency,verbose,'','','')
                merge_nextdep_jobid=merge_job.submit()
                count=count+1
                if count == testtimes:
                        break

def SOAP(argv,MaxWaitingJobNumber):
        import os,subprocess,sys,csv
        from popen2 import popen2
        import time
        import getpass
        import pandas as pd
        import glob
        import NCHC.step.step as step
        import NCHC.workflow.workflow as workflow
        import NCHC.queue.pbs as pbs
        import getpass
        from NCHC.queue.pbs import Job
        username = getpass.getuser()
        
        #argv=sys.argv
        #fasPropionatetqdumpPath='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        script_path = os.path.dirname(os.path.realpath(__file__))
        pfastqdump_path='/pkg/biology/pfastq-dump/pfastqdump_v0.1.6/bin/pfastq-dump' #path
        fastqdump_path='/pkg/biology/SRA_Toolkit/SRAToolkit_v2.10.2/bin/fastq-dump'
        DiamondDB_path='/pkg/biology/Run_SCFA/runscfa_20200115/SOAP_DB/ETBF/ETBF.fasta.index'
        SOAP_path='/pkg/biology/Run_SCFA/runscfa_20200115/bin_SOAP/Run_SOAP_pipline.py'
        verbose=False
        thread=10
        outdir=os.getcwd()
        inputdir=os.getcwd()
        projectID='MST108173'
        sraprojectID='ERP012177'
        runIDs=[]
        testtimes=0
        resume=False
        if '--thread' in argv:
                thread=argv[argv.index('--thread')+1]
        if '--outdir' in argv:
                outdir=argv[argv.index('--outdir')+1]
        if outdir=='./':
                outdir=os.getcwd()
        if '--project' in argv:
                projectID=argv[argv.index('--project')+1]
                #download run lists from projectID
        if '--sraproject' in argv:
                sraprojectID=argv[argv.index('--sraproject')+1]
                outdir=outdir+'/'+sraprojectID.strip()
                os.system('mkdir -p %s' % outdir)
                os.chdir(outdir)
        if '--test' in argv:
                #        testtimes=int(argv[argv.index('--test')+1])
                testtimes=2
                print 'Test mode: Times '+str(testtimes)
        if '--runids' in argv:
                runIDs=argv[argv.index('--runids')+1].split(',')
                print runIDs
        if '--resume' in argv:
                resume=True
        if '-v' in argv:
                verbose=True
        if '--inputdir' in argv:
                inputdir=os.path.abspath(argv[argv.index('--inputdir')+1])

        if '--help' in argv:
                print """
USAGE:
        pipeline.py --sraproject [NCBI Project ID]
OPTIONS:
        --sraproject
            Project ID in NCBI format.
        --runids
            Only these run IDs will be run if this option presents.
        --outdir
            Output directory where the results locate. Default is current working directory
        --project
            Project ID for submitting jobs. Default: MST108173
        --test
            Testing mode: Only one run will be run for testing.
        --SOAPonly
            Run Propionateonly,skipping SRA download and fastqdump.
        --inputdir
            The input directory of data that is self-uploaded (not from NCBI or EBI)
        --resume
            resume the unfinished or redo the error jobs
        -v
            Verbose

EXAMPLES:
        If data is from a ncbi project:
            pipeline.py --sraproject ERP012177
        To run only one run for testing:
            pipeline.py --sraproject ERP012177 --test
        If only a run in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197
        If two runs in a project to be run:
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196
        If only the Propionatestep to be run( skipping download and fastqdump):
            pipeline.py --sraproject ERP012177 --runids ERR1018197,ERR1018196 --SOAPonly
        If input data is self-uploaded, not from NCBI or EBI. Each run's fastq.gz should be put into a directory named with its run ID.
            pipeline.py --inputdir [data directory]

        """

                sys.exit(0)
        if '--inputdir' not in argv and '--sraproject' not in argv:
                print "Please specify at least --inputdir or --SOAPonly"
                sys.exit(0)
       
        if '--inputdir' in argv:
                workflow.SOAP_only_wf(inputdir,outdir,thread,DiamondDB_path,verbose,MaxWaitingJobNumber)
                sys.exit(0)
        
        #getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s[BioProject]\' -O -' % (sraprojectID)
        getlistcmd='wget \'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&rettype=runinfo&db=sra&term=%s\' -O -' % (sraprojectID)
        list = subprocess.check_output(getlistcmd, shell=True).splitlines()
        runsReader=csv.DictReader(list)
        download_jobids=[]
        all_jobids=[]
        count=0
        download_thread=6 #number of download jobs at the same time
        for run in runsReader:
                soap_result_folder='%s/results_soap' % outdir
                logdir=outdir+'/logs'
                if runIDs and run['Run'] not in runIDs:
                        continue
                if '--SOAPonly' in argv:
                        print 'submitting jobs for %s' % run['Run']
                        SOAP_job=Job('ngs48G',projectID,run['Run']+'.3.run_SOAP.SOAP',logdir,step.SOAP(SOAP_path,outdir+'/runs/'+run['Run'],soap_result_folder+'/'+run['Run'],thread,DiamondDB_path),'',verbose,MaxWaitingJobNumber,'',resume)
                        SOAP_jobid=SOAP_job.submit()
                        all_jobids.append(SOAP_jobid)
                        merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/BFT_result.txt\' --outfile %s/BFT_result.txt --sortindex %s \n' % (script_path,soap_result_folder,outdir,'sample')
                        merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.SOAP',logdir,merge_CMD,{"jobs":SOAP_jobid,"type":"afterany"},verbose,'','','')
                        merge_jobid=merge_job.submit()
                        count=count+1
                        if count == testtimes:
                                break
                        continue
                            
###download sample
                dep_index=count-download_thread

                print 'submitting jobs for %s' % run['Run']
                
                #following while loop should be used while dependency issue hasn't been fixed.
             
                if dep_index <0:
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.SOAP',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)
                else:
                        download_job=Job('ngs4G',projectID,run['Run']+'.1.download.SOAP',logdir,step.downloadrun(run,outdir),'',verbose,MaxWaitingJobNumber,'',resume)
                        while download_job.getjobnumber('\'\.1\.\'') > download_thread:
                                time.sleep(10)

                download_nextdep_jobid=download_job.submit()
                last_download_jobid=download_nextdep_jobid
                download_jobids.append(download_nextdep_jobid)
                
                ###if download finished,then submitjob


                ####fastqdump ID
                
                dependency={"jobs":download_nextdep_jobid,"type":"afterok"}
                fastqdump_cmd=step.fastqdump(fastqdump_path,run,outdir,thread)
                pfastqdump_cmd=step.pfastqdump(pfastqdump_path,run,outdir,thread)
                fastqdump_job=Job('ngs16G',projectID,run['Run']+'.2.fastqdump.SOAP',logdir,fastqdump_cmd,dependency,verbose,'',download_job,resume)
                fastqdump_nextdep_jobid=fastqdump_job.submit()
                if '--downloadonly' in argv:  all_jobids.append(fastqdump_nextdep_jobid); continue
                               
                ####SOAP ID

                dependency={"jobs":fastqdump_nextdep_jobid,"type":"afterok"}
                SOAP_job=Job('ngs48G',projectID,run['Run']+'.3.run_SOAP.SOAP',logdir,step.SOAP(SOAP_path,outdir+'/runs/'+run['Run'],soap_result_folder+'/'+run['Run'],thread,DiamondDB_path),dependency,verbose,'',fastqdump_job,resume)
                SOAP_nextdep_jobid=SOAP_job.submit()
                print "SOAP jobid: "+SOAP_nextdep_jobid
                all_jobids.append(SOAP_nextdep_jobid)
                


                ####merge results
                merge_CMD='%s/../../scripts/mergeresults.py --filepath \'%s/*/BFT_result.txt\' --outfile %s/BFT_result.txt --sortindex %s \n' % (script_path,soap_result_folder,outdir,'sample')
                all_jobids=filter(None,all_jobids)
#                dependency={"jobs":all_jobids,"type":"afterany"}
                dependency={"jobs":SOAP_nextdep_jobid,"type":"afterany"}
                merge_job=Job('ngs4G',projectID,run['Run']+'.4.Merge_results.SOAP',logdir,merge_CMD,dependency,verbose,'','','')
                merge_nextdep_jobid=merge_job.submit()

                count=count+1
                if count == testtimes:
                        break

