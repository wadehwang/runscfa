#!/pkg/biology/Python/Python2_default/bin/python
import pandas as pd
import numpy as np
import glob
import sys,os
argv=sys.argv
outfile=''
sort_index=''
def main():
        outdir=os.getcwd()
        filepath_pattern='*/result.txt'
        outfile='./result.txt'
        sort_index='sample'
        if '--filepath' in argv:
                filepath_pattern=argv[argv.index('--filepath')+1]
        if '--outfile' in argv:
                outfile=argv[argv.index('--outfile')+1]
        if '--sortindex' in argv:
                sort_index=argv[argv.index('--sortindex')+1]

        merge_results(filepath_pattern,sort_index,outfile)       
        
def merge_results(filepath_pattern,sort_index,outputfile):
        files=glob.glob(filepath_pattern)
        output=pd.DataFrame()
        for file in files:
                record=pd.read_table(file)
                if len(record.index)==0:
                        record.loc[0,:]=np.nan
                if 'Filename' not in record.columns:        
                        record.insert(0,'Filename',file)
                else:
                        record.loc[0,'Filename']=file
                output=pd.concat([output,record],ignore_index=True,sort=False)
        #print output
        output=output.sort_values(sort_index)
        output.to_csv(outputfile,sep="\t",index=False)

main()
